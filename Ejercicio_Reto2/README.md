### UD_C1: DML – Consultas Reto 2: Consultas básicas II

## Ejercicio 2

Hecho por: David Barberá Muñoz 

Si quieres ver el repositorio, dale click aqui https://gitlab.com/daviid45/dam-bases/-/tree/main/Ejercicio_Reto2

### Empresa

#### 1. Muestre los productos (código y descripción) que comercializa la empresa.
```
use empresa;
select PROD_NUM, DESCRIPCIO 
from PRODUCTE
```

#### 2. Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.
```
use empresa;
select PROD_NUM, DESCRIPCIO 
from PRODUCTE
where DESCRIPCIO like '%tennis%'
```

#### 3. Muestre el código, nombre, área y teléfono de los clientes de la empresa.
```
use empresa;
select CLIENT_COD, NOM, AREA, TELEFON 
from CLIENT
```

#### 4. Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.
```
use empresa;
select PROD_NUM, DESCRIPCIO 
from PRODUCTE
```

#### 5. Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío)
```
use empresa;
select PROD_NUM, DESCRIPCIO 
from PRODUCTE
```

### Videoclub

#### 6. Lista de nombres y teléfonos de los clientes.
```
use videoclub;
select Nom, Telefon
from CLIENT
```

#### 7. Lista de fechas e importes de las facturas.
```
use videoclub;
select Data, Import
from FACTURA
```

#### 8. Lista de productos (descripción) facturados en la factura número 3.
```
use videoclub;
select Descripcio
from DETALLFACTURA
where CodiFactura like '3'
```

#### 9. Lista de facturas ordenada de forma decreciente por importe.
```
use videoclub;
select *
from FACTURA
order by Import desc
```

#### 10. Lista de los actores cuyo nombre comience por X.
```
use videoclub;
select Nom
from ACTOR
where Nom like 'X%'
```

#### 11. Añadir actores que empiezen con la letra X.
```
use videoclub;
insert into ACTOR (Nom, CodiActor)
values ("Xordi", "6");
```

#### 12. Eliminamos a Xordi.
```
use videoclub;
delete from ACTOR
where Nom = "Xordi"
```


#### 13. Añadir dos actores que empiezan con la letra X
```
use videoclub;
insert into ACTOR (Nom, CodiActor)
values ("Xordi", "6"), 
("Ximo", "7");
```

#### 14. Eliminamos a Actores
```
use videoclub;
delete from ACTOR
where Nom = "Xordi" or Nom = "Ximo"
```

#### 15. Cambiar un registro sin eliminarlo 
```
use videoclub;
update ACTOR
set Nom = "Jordi"
where Nom = "Xordi";
```