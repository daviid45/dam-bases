USE Chinook;
SELECT *
FROM Invoice I 
INNER JOIN Customer C ON I.CustomerId = C.CustomerId
WHERE C.Email LIKE 'emma_jones@hotmail.com'
ORDER BY I.InvoiceDate DESC
LIMIT 5;