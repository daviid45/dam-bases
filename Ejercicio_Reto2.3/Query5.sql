USE Chinook;
SELECT A.Title AS Album, Ar.Name AS Artist
FROM Album A
INNER JOIN Artist Ar ON A.ArtistId = Ar.ArtistId
INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
HAVING COUNT(T.TrackId) > 15;
