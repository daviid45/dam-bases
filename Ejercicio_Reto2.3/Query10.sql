USE Chinook;
SELECT C.FirstName, COUNT(I.Total)
FROM Customer C 
inner join Invoice I on C.CustomerId = I.CustomerId
GROUP BY C.FirstName
ORDER BY COUNT(I.Total) DESC