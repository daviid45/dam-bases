USE Chinook;
SELECT A.Title AS Nombre_Álbum,
(SELECT COUNT(TrackId)
FROM Track T
WHERE T.AlbumId = A.AlbumId) AS Canciones

FROM Album A
ORDER BY Canciones DESC

