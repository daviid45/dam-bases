USE Chinook;

SELECT G.Name AS Genre, COUNT(T.TrackId) AS NumSongs
FROM Track T
INNER JOIN Genre G ON T.GenreId = G.GenreId
GROUP BY G.Name
ORDER BY COUNT(T.TrackId) DESC
LIMIT 1;


