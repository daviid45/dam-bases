USE Chinook;
SELECT A.Title AS Album, COUNT(T.TrackId) AS NumTracks
FROM Album A
INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING COUNT(T.TrackId) > (SELECT AVG(NumTracks) FROM (SELECT COUNT(TrackId) AS NumTracks FROM Track GROUP BY AlbumId) AS TrackCounts);
