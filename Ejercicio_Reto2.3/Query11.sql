USE Chinook;
SELECT concat(E.FirstName, ' ' ,E.LastName) AS Nombre_Completo,
(SELECT COUNT(CustomerId)
FROM Customer 
WHERE SupportRepId = E.EmployeeId ) AS Cantidad_Empleados

FROM Employee E
WHERE E.Title like 'Sales Support Agent';