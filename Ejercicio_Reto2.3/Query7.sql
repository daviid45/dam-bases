USE Chinook;
SELECT A.Title AS Album, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING SUM(T.Milliseconds) > (SELECT AVG(Milliseconds) FROM Track)
ORDER BY DuracionTotal DESC;
