# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Si quiere4s verlo en gitlab, dale click aqui https://gitlab.com/daviid45/dam-bases/-/blob/main/Ejercicio_Reto2.3/README(5).md

### Consulta 1: Obtener las canciones con una duración superior a la media.
```sql
USE Chinook;
SELECT Name, Milliseconds
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM Track)
ORDER BY Milliseconds DESC
```

### Consulta 2: Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".
```sql
USE Chinook;
SELECT *
FROM Invoice I 
INNER JOIN Customer C ON I.CustomerId = C.CustomerId
WHERE C.Email LIKE 'emma_jones@hotmail.com'
ORDER BY I.InvoiceDate DESC
LIMIT 5;
```

### Consulta 3: Mostrar las listas de reproducción en las que hay canciones de reggae.
```sql
USE Chinook;
SELECT *
FROM Track T 
INNER JOIN Genre G ON T.GenreId = G.GenreId
WHERE G.Name LIKE 'Reggae'
```

### Consulta 4: Obtener la información de los clientes que han realizado compras superiores a 20€.
```sql
USE Chinook;
SELECT *
FROM Invoice I 
INNER JOIN Customer C ON I.CustomerId = C.CustomerId
WHERE I.Total > '20'
```

### Consulta 5: Álbumes que tienen más de 15 canciones, junto a su artista.
```sql
USE Chinook;
SELECT A.Title AS Album, Ar.Name AS Artist
FROM Album A
INNER JOIN Artist Ar ON A.ArtistId = Ar.ArtistId
INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
HAVING COUNT(T.TrackId) > 15;
```

### Consulta 6: Obtener los álbumes con un número de canciones superiores a la media.
```sql
USE Chinook;
SELECT A.Title AS Album, COUNT(T.TrackId) AS NumTracks
FROM Album A
INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING COUNT(T.TrackId) > (SELECT AVG(NumTracks) FROM (SELECT COUNT(TrackId) AS NumTracks FROM Track GROUP BY AlbumId) AS TrackCounts);
```

### Consulta 7: Obtener los álbumes con una duración total superior a la media.
```sql
USE Chinook;
SELECT A.Title AS Album, SUM(T.Milliseconds) AS DuracionTotal
FROM Album A
INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId, A.Title
HAVING SUM(T.Milliseconds) > (SELECT AVG(Milliseconds) FROM Track)
ORDER BY DuracionTotal DESC;
```

### Consulta 8: Canciones del género con más canciones.
```sql
USE Chinook;
SELECT G.Name AS Genre, COUNT(T.TrackId) AS NumSongs
FROM Track T
INNER JOIN Genre G ON T.GenreId = G.GenreId
GROUP BY G.Name
ORDER BY COUNT(T.TrackId) DESC
LIMIT 1;
```

### Consulta 9: Canciones de la _playlist_ con más canciones.
```sql
USE Chinook;
SELECT PlaylistId, COUNT(TrackId) AS NumSongs
FROM PlaylistTrack
GROUP BY PlaylistId
ORDER BY COUNT(TrackId) DESC
LIMIT 1;
```

### Consulta 10: Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
```sql
USE Chinook;
SELECT C.FirstName, COUNT(I.Total)
FROM Customer C 
inner join Invoice I on C.CustomerId = I.CustomerId
GROUP BY C.FirstName
ORDER BY COUNT(I.Total) DESC
```

### Consulta 11: Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
```sql
USE Chinook;
SELECT concat(E.FirstName, ' ' ,E.LastName) AS Nombre_Completo,
(SELECT COUNT(CustomerId)
FROM Customer 
WHERE SupportRepId = E.EmployeeId ) AS Cantidad_Empleados

FROM Employee E
WHERE E.Title like 'Sales Support Agent';
```
### Consulta 12: Ventas totales de cada empleado.
```sql
USE Chinook;
SELECT concat(E.FirstName, ' ' ,E.LastName) AS Nombre_Completo, SUM(I.Total) AS Ventas
FROM Employee E
INNER JOIN Customer C ON E.EmployeeId = C.SupportRepId
INNER JOIN Invoice I ON C.CustomerId = I.CustomerId
GROUP BY E.EmployeeId
```
### Consulta 13: Álbumes junto al número de canciones en cada uno.
```sql
USE Chinook;
SELECT A.Title AS Nombre_Álbum,
(SELECT COUNT(TrackId)
FROM Track T
WHERE T.AlbumId = A.AlbumId) AS Canciones

FROM Album A
ORDER BY Canciones DESC
```
### Consulta 14: Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.
```sql
USE Chinook;
SELECT A.Name AS Nombre_Artista, MAX(Al.Title) Álbum_Más_Reciente
FROM Artist A
INNER JOIN Album Al ON A.ArtistId = Al.ArtistId
GROUP BY A.ArtistId
```