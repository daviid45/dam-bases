USE Chinook;
SELECT concat(E.FirstName, ' ' ,E.LastName) AS Nombre_Completo, SUM(I.Total) AS Ventas
FROM Employee E
INNER JOIN Customer C ON E.EmployeeId = C.SupportRepId
INNER JOIN Invoice I ON C.CustomerId = I.CustomerId
GROUP BY E.EmployeeId