USE Chinook;
SELECT A.Name AS Nombre_Artista, MAX(Al.Title) Álbum_Más_Reciente
FROM Artist A
INNER JOIN Album Al ON A.ArtistId = Al.ArtistId
GROUP BY A.ArtistId