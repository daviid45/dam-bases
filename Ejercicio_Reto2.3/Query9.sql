USE Chinook;
SELECT PlaylistId, COUNT(TrackId) AS NumSongs
FROM PlaylistTrack
GROUP BY PlaylistId
ORDER BY COUNT(TrackId) DESC
LIMIT 1;