USE Chinook;
SELECT C.LastName as Apellidos, I.Total as Importe
FROM Customer C INNER JOIN Invoice I ON C.CustomerId = I.CustomerId
WHERE I.Total > 10
ORDER BY C.LastName ASC

