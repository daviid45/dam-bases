### Reto 2.1: Consultas con funciones de agregación

## Ejercicio 2.1

Hecho por: David Barberá Muñoz 

Si quieres ver el repositorio, dale click aqui https://gitlab.com/daviid45/dam-bases/-/tree/main/Ejercicio_Reto2.1

### Chinook

### Consultas sobre una tabla

#### Query 1: Encuentra todos los clientes de Francia.
```sql
USE Chinook;
SELECT * 
FROM Customer
WHERE Country = 'France';
```

#### Query 2: Muestra las facturas del primer trimestre de este año.
```sql
USE Chinook;
SELECT * 
FROM Invoice
WHERE InvoiceDate between '2024-01-01' and '2024-03-31';
```

#### Query 3: Muestra todas las canciones compuestas por AC/DC.
```sql
USE Chinook;
SELECT * 
FROM Track
WHERE Composer = 'AC/DC'
```

#### Query 4: Muestra las 10 canciones que más tamaño ocupan.
```sql
USE Chinook;
SELECT * 
FROM Track
ORDER BY Bytes DESC
LIMIT 10
```

#### Query 5: Muestra el nombre de aquellos países en los que tenemos clientes.
```sql
USE Chinook;
SELECT DISTINCT(Country)
FROM Customer
```

#### Query 6: Muestra todos los géneros musicales.
```sql
USE Chinook;
SELECT *
FROM Genre
```

#### Query 7: Muestra todos los artistas junto a sus álbumes.
```sql
USE Chinook;
SELECT *
FROM Artist A inner join Album Al on A.ArtistId = Al.ArtistId
```

#### Query 8: Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.
```sql
USE Chinook;
SELECT * 
FROM Employee E INNER JOIN Employee E2 ON E.EmployeeId = E2.ReportsTo
ORDER BY E.BirthDate DESC
LIMIT 15
```

#### Query 9: Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación, código postal, país, importe (en este orden).
```sql
USE Chinook;
SELECT I.InvoiceDate as Fecha_Factura, CONCAT(C.FirstName," ",C.LastName) as Nombre_Cliente, I.BillingAddress as Dirección_Facturación, C.PostalCode as Código_Pstal, C.Country as Pais, I.Total as Importe 
FROM Invoice I INNER JOIN Customer C ON I.CustomerId = C.CustomerId
WHERE City like 'Berlin'
```

#### Query 10: Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.
```sql
USE Chinook;
SELECT Pla.Name as Nombre_Album, Tr.Name as Nombre_Canción, Tr.Milliseconds as Milisegundos
FROM Album A INNER JOIN Track Tr ON A.AlbumId = Tr.AlbumId
INNER JOIN PlaylistTrack Pl ON Tr.TrackId = Pl.TrackId
INNER JOIN Playlist Pla ON Pl.PlaylistId = Pla.PlaylistId
WHERE Pla.Name LIKE 'C%'
ORDER BY Tr.Milliseconds DESC;
```

#### Query 11: Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.
```sql
USE Chinook;
SELECT C.LastName as Apellidos, I.Total as Importe
FROM Customer C INNER JOIN Invoice I ON C.CustomerId = I.CustomerId
WHERE I.Total > 10
ORDER BY C.LastName ASC
```

### Consultas con funciones de agregación

#### Query 12: Muestra el importe medio, mínimo y máximo de cada factura.
```sql
USE Chinook;
SELECT min(Total) as Mínimo, max(Total) as Máximo, avg(Invoice.Total) as Media
FROM Invoice;
```

#### Query 13: Muestra el número total de artistas.
```sql
USE Chinook;
SELECT count(ArtistId) as Nº_Artistas
FROM Artist;
```

#### Query 14: Muestra el número de canciones del álbum “Out Of Time”.
```sql
USE Chinook;
SELECT count(T.Name) as Nº_Canciones
FROM Album A INNER JOIN Track T ON A.AlbumId = T.AlbumId
WHERE A.Title like 'Out Of Time'
```

#### Query 15: Muestra el número de países donde tenemos clientes.
```sql
USE Chinook;
SELECT count(DISTINCT Country) as Nº_Paises
FROM Customer
```

#### Query 16: Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).
```sql
USE Chinook;
SELECT G.Name AS Genero, COUNT(T.TrackId) AS NumeroDeCanciones
FROM Genre G INNER JOIN Track T ON G.GenreId = T.GenreId
GROUP BY G.Name;
```

#### Query 17: Muestra los álbumes ordenados por el número de canciones que tiene cada uno.
```sql
USE Chinook;
SELECT A.Title AS Nombre_Album, COUNT(T.TrackId) AS Numero_Canciones
FROM Album A INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY COUNT(T.TrackId) DESC;
```

#### Query 18: Encuentra los géneros musicales más populares (los más comprados).
```sql
USE Chinook;
SELECT G.Name AS Genero, SUM(T.UnitPrice) AS Total_Comprado
FROM Genre G INNER JOIN Track T ON G.GenreId = T.GenreId
INNER JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
GROUP BY G.Name
ORDER BY SUM(T.UnitPrice) DESC;
```

#### Query 19: Lista los 6 álbumes que acumulan más compras.
```sql
USE Chinook;
SELECT G.Name AS Álbumes, SUM(IL.Quantity) AS Total_Comprado
FROM Genre G INNER JOIN Track T ON G.GenreId = T.GenreId
INNER JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
GROUP BY G.Name
ORDER BY SUM(IL.Quantity) DESC
LIMIT 6;
```

#### Query 20: Muestra los países en los que tenemos al menos 5 clientes.
```sql
USE Chinook;
SELECT Country, COUNT(CustomerId) AS Clientes
FROM Customer
GROUP BY Country
HAVING COUNT(CustomerId) >= 5
ORDER BY COUNT(CustomerId) DESC;
```

HAVING: Se utiliza junto con la cláusula GROUP BY para filtrar resultados basados en una condición agregada (como contar, sumar, etc.) después de que se han agrupado los datos.
