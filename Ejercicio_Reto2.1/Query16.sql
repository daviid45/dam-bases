USE Chinook;
SELECT G.Name AS Genero, COUNT(T.TrackId) AS NumeroDeCanciones
FROM Genre G INNER JOIN Track T ON G.GenreId = T.GenreId
GROUP BY G.Name;
