USE Chinook;
SELECT G.Name AS Genero, SUM(T.UnitPrice) AS Total_Comprado
FROM Genre G INNER JOIN Track T ON G.GenreId = T.GenreId
INNER JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
GROUP BY G.Name
ORDER BY SUM(T.UnitPrice) DESC;
