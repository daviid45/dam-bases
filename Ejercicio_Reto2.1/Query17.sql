USE Chinook;
SELECT A.Title AS Nombre_Album, COUNT(T.TrackId) AS Numero_de_Canciones
FROM Album A
INNER JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY COUNT(T.TrackId) DESC;
