USE Chinook;
SELECT I.InvoiceDate as Fecha_Factura, CONCAT(C.FirstName," ",C.LastName) as Nombre_Cliente, I.BillingAddress as Dirección_Facturación, C.PostalCode as Código_Pstal, C.Country as Pais, I.Total as Importe 
FROM Invoice I INNER JOIN Customer C ON I.CustomerId = C.CustomerId
WHERE City like 'Berlin'
