USE Chinook;
SELECT Pla.Name as Nombre_Album, Tr.Name as Nombre_Canción, Tr.Milliseconds as Milisegundos
FROM Album A INNER JOIN Track Tr ON A.AlbumId = Tr.AlbumId
INNER JOIN PlaylistTrack Pl ON Tr.TrackId = Pl.TrackId
INNER JOIN Playlist Pla ON Pl.PlaylistId = Pla.PlaylistId
WHERE Pla.Name LIKE 'C%'
ORDER BY Tr.Milliseconds DESC;

