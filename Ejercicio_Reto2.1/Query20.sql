USE Chinook;
SELECT Country, COUNT(CustomerId) AS Clientes
FROM Customer
GROUP BY Country
HAVING COUNT(CustomerId) >= 5
ORDER BY COUNT(CustomerId) DESC;
