USE Chinook;
SELECT G.Name AS Álbumes, SUM(IL.Quantity) AS Total_Comprado
FROM Genre G INNER JOIN Track T ON G.GenreId = T.GenreId
INNER JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
GROUP BY G.Name
ORDER BY SUM(IL.Quantity) DESC
LIMIT 6;