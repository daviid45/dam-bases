### Definición de datos en MySQL
#### Alumno: David Barberá Muñoz

Si quieres ver el repositorio, dale click aqui https://gitlab.com/daviid45/dam-bases/-/tree/main/Ejercicio_Reto3.1

#### Creación de Base de Datos
Con este comando creamos la base de datos `reservaVuelos`, en esta base de datos guardaremos y crearemos todos los datos necesarios junto con sus tablas:

```sql
CREATE DATABASE reservaVuelos.sql
```

#### Creación de tabla Pasajeros
Una de sus tablas es `Pasajeros`, indicara su `id_pasajero`, `numero_pasaporte` y el `nombre_pasajero` cada dato de la tabla tiene sus características, indicamos `id_pasajero` y el `numero_pasaporte` como PRIMARY KEY ya que con el id y con el número del pasaporte podemos saber quienes son e identificarlos con más facilidad, luego he puesto `AUTO_INCREMENT` en `id_pasajero` para que por cada pasajero creado se le asigne un id automaticamente:
```sql
CREATE TABLE `Pasajeros` (
  `id_pasajero` int unsigned NOT NULL AUTO_INCREMENT,
  `numero_pasaporte` varchar(50) NOT NULL DEFAULT '',
  `nombre_pasajero` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_pasajero`,`numero_pasaporte`)
);
```

#### Insercción de datos en la tabla Pasajeros
```sql
INSERT INTO `Pasajeros` VALUES (1,'AB123456','John Smith'),(2,'CD789012','Emma Johnson'),(3,'EF345678','Michael Brown'),(4,'GH901234','Emily Williams'),(5,'IJ567890','William Davis'),(6,'KL123456','Olivia Jones'),(7,'MN789012','James Wilson'),(8,'OP345678','Sophia Taylor'),(9,'QR901234','Alexander Martinez'),(10,'ST567890','Charlotte Garcia'),(11,'UV123456','Daniel Rodriguez'),(12,'WX789012','Isabella Hernandez'),(13,'YZ345678','Matthew Lopez'),(14,'AB901234','Amelia Gonzalez'),(15,'CD567890','Ethan Perez'),(16,'EF123456','Mia Sanchez'),(17,'GH789012','Benjamin Rivera'),(18,'IJ345678','Evelyn Torres'),(19,'KL901234','Andrew Flores'),(20,'MN567890','Avery Ramirez'),(21,'OP123456','Sofia Cruz'),(22,'QR789012','Jackson Moore'),(23,'ST345678','Ava Gomez'),(24,'UV901234','Lucas Reed'),(25,'WX567890','Harper Price'),(26,'YZ123456','Jackson Long'),(27,'AB789012','Scarlett King'),(28,'CD345678','Henry Young'),(29,'EF901234','Madison Bell'),(30,'GH567890','Carter Howard'),(31,'IJ123456','Victoria Cook'),(32,'KL789012','Mateo Baker'),(33,'MN345678','Penelope Turner'),(34,'OP901234','Gabriel Adams'),(35,'QR567890','Grace Morris'),(36,'ST123456','Ryan Hill'),(37,'UV789012','Luna Mitchell'),(38,'WX345678','Leo Perez'),(39,'YZ901234','Liam Thompson'),(40,'AB567890','Zoe Nelson'),(41,'CD123456','Elijah White'),(42,'EF789012','Lily Martin'),(43,'GH345678','Nathan Scott'),(44,'IJ901234','Audrey Green'),(45,'KL567890','Joseph Baker'),(46,'MN123456','Stella Martinez'),(47,'OP789012','David Nguyen'),(48,'QR345678','Aria Carter'),(49,'ST901234','Jack Hill'),(50,'UV567890','Emma Phillips'),(51,'PO987564','Pablo Ruíz'),(52,'dsfxfced444','DAvid'),(53,'sddfr3344','David Bar');
```

#### Creación de tabla Vuelos
La segunda tabla es la tabla `Vuelos`, `id_vuelo` indica el número del vuelo creado, también con `AUTO_INCREMENT` para que sea más fácil su creación, el `origen` da información desde donde sale el vuelo, el `destino` da información de donde aterriza el avión, la `fecha` en tipo date (yyyy-mm-dd) indica cuando saldrá el avión, la `capacidad` indica la capacidad del avión.
Le e puesto PRIMARY KEY en `id_vuelo` para luego gestionar mejor el vuelo.
```sql
CREATE TABLE `Vuelos` (
  `id_vuelo` int unsigned NOT NULL AUTO_INCREMENT,
  `origen` varchar(50) NOT NULL DEFAULT '',
  `destino` varchar(50) NOT NULL DEFAULT '',
  `fecha` date NOT NULL DEFAULT (0),
  `capacidad` int NOT NULL DEFAULT (0),
  PRIMARY KEY (`id_vuelo`)
);
```

#### Insercción de datos en la tabla Vuelos
```sql
INSERT INTO `Vuelos` VALUES (1,'New York','Los Angeles','2024-03-21',200),(2,'Los Angeles','Chicago','2024-03-22',180),(3,'Chicago','Houston','2024-03-23',220),(4,'Houston','Miami','2024-03-24',190),(5,'Miami','Denver','2024-03-25',210),(6,'Denver','San Francisco','2024-03-26',180),(7,'San Francisco','Seattle','2024-03-27',220),(8,'Seattle','Las Vegas','2024-03-28',200),(9,'Las Vegas','New York','2024-03-29',190),(10,'New York','Dallas','2024-03-30',220),(11,'Dallas','Atlanta','2024-03-31',210),(12,'Atlanta','Phoenix','2024-04-01',180),(13,'Phoenix','Boston','2024-04-02',200),(14,'Boston','Washington DC','2024-04-03',190),(15,'Washington DC','San Diego','2024-04-04',220),(16,'San Diego','Minneapolis','2024-04-05',210),(17,'Minneapolis','Detroit','2024-04-06',180),(18,'Detroit','Philadelphia','2024-04-07',200),(19,'Philadelphia','San Antonio','2024-04-08',190),(20,'San Antonio','Portland','2024-04-09',220),(21,'Portland','St. Louis','2024-04-10',210),(22,'St. Louis','Charlotte','2024-04-11',180),(23,'Charlotte','Orlando','2024-04-12',200),(24,'Orlando','Salt Lake City','2024-04-13',190),(25,'Salt Lake City','Tampa','2024-04-14',220),(26,'Tampa','Cincinnati','2024-04-15',210),(27,'Cincinnati','Kansas City','2024-04-16',180),(28,'Kansas City','Indianapolis','2024-04-17',200),(29,'Indianapolis','Raleigh','2024-04-18',190),(30,'Raleigh','Nashville','2024-04-19',220),(31,'Nashville','Las Vegas','2024-04-20',210),(32,'Las Vegas','San Francisco','2024-04-21',180),(33,'San Francisco','Chicago','2024-04-22',200),(34,'Chicago','New York','2024-04-23',190),(35,'New York','Los Angeles','2024-04-24',220),(36,'Los Angeles','Miami','2024-04-25',210),(37,'Miami','Dallas','2024-04-26',180),(38,'Dallas','Seattle','2024-04-27',200),(39,'Seattle','Boston','2024-04-28',190),(40,'Boston','Phoenix','2024-04-29',220),(41,'Phoenix','Washington DC','2024-04-30',210),(42,'Washington DC','San Diego','2024-05-01',180),(43,'San Diego','Detroit','2024-05-02',200),(44,'Detroit','Portland','2024-05-03',190),(45,'Portland','Charlotte','2024-05-04',220),(46,'Charlotte','Salt Lake City','2024-05-05',210),(47,'Salt Lake City','Tampa','2024-05-06',180),(48,'Tampa','Cincinnati','2024-05-07',200),(49,'Cincinnati','Kansas City','2024-05-08',190),(50,'Kansas City','Indianapolis','2024-05-09',220),(51,'Valencia','Barcelona','2024-04-01',200),(52,'Valencia','Madrid','2024-12-04',233),(53,'Mallorca','tt','2024-12-12',250),(54,'Valecia','Madrid','2024-12-09',100);
```

#### Creación de tabla Vuelos_Pasajeros
La tercera y última tabla es la que relaciona los vuelos con los pasajeros, esta tabla permite ver el `id_vuelo` junto con el `id_pasajero`, esa relación permite ver que pasajero a cogido su vuelo junto con su `n_asiento`, también tiene un `id_reserva` que es la PRIMARY KEY de la tabla está puesta con `AUTO_INCREMENT` debido a que por cada reserva realizada se incremente en la tabla y asi se ve reflejado.
```sql
CREATE TABLE `Vuelos_Pasajeros` (
  `id_reserva` int NOT NULL AUTO_INCREMENT,
  `id_vuelo` int unsigned NOT NULL,
  `id_pasajero` int unsigned NOT NULL,
  `n_asiento` int DEFAULT NULL,
  PRIMARY KEY (`id_reserva`),
  KEY `id_vuelo` (`id_vuelo`),
  KEY `id_pasajero` (`id_pasajero`),
  CONSTRAINT `id_pasajero` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`),
  CONSTRAINT `id_vuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`)
);
```

#### Insercción de datos en la tabla Vuelos_Pasajeros
```sql
INSERT INTO `Vuelos_Pasajeros` VALUES (2,2,2,5),(3,3,3,2),(5,5,5,21),(6,6,6,10),(7,7,7,15),(8,8,8,9),(9,9,9,24),(10,10,10,3),(11,11,11,16),(12,12,12,7),(13,13,13,20),(14,14,14,11),(15,15,15,14),(16,16,16,19),(17,17,17,2),(18,18,18,25),(19,19,19,6),(20,20,20,13),(21,21,21,12),(22,22,22,1),(23,23,23,17),(24,24,24,4),(25,25,25,23),(26,26,26,12),(27,27,27,5),(28,28,28,2),(29,29,29,8),(30,30,30,21),(31,31,31,10),(32,32,32,15),(34,34,34,24),(35,35,35,3),(36,36,36,16),(37,37,37,7),(38,38,38,20),(39,39,39,11),(40,40,40,14),(41,41,41,19),(42,42,42,2),(43,43,43,25),(45,45,45,13),(46,46,46,12),(47,47,47,1),(48,48,48,17),(49,49,49,4),(50,50,50,23),(51,32,1,2);
```

## Preguntas

#### ¿Por qué necesitamos tres tablas?

Necesitamos 3 tablas principalmente primero para identificar los vuelos disponibles, luego para obviamnete coger al pasajero que va a volar y luego la ultima tabla y la más importante para relacionar el vuelo con el pasajero que volará, por eso es importante una tercera tabla por el `id_reserva`.

#### ¿Cuáles son las claves primarias y foráneas?

Tabla Pasajeros: PRIMARY KEY (`id_pasajero`,`numero_pasaporte`)

Tabla Vuelos: PRIMARY KEY (`id_vuelo`)

Tabla Vuelos_Pasajeros: PRIMARY KEY (`id_reserva`) y las claves foráneas CONSTRAINT `id_vuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`)

#### ¿Por qué utilizamos las restricciones que hemos definido y no otras?

He utilizado esas restricciones y no otras porque creo que son las mejores para este tipo de cosas siempre pensando en la fluidez de la base de datos y en la interacción con la base de datos.

#### ¿Qué otras situaciones sería conveniente que manejara el SGBD para asegurar la integridad de los datos?

Una de las meiores situaciones para mejorar la integridad de los datos es poner atributos a los datos especializados para ese tipo caso, no poner un varchar(500) si se pide un nombre, no malgastar memoria.

#### Cómo explorar las bases de datos del SGBD

Para poder explorar las bases de datos podemos usar el comando `SHOW DATABASES` te saldrán todas las bases de datos creadas.

#### Cómo explorar las tablas de una BD

Primero debes expecificar la base de datos que usarás, en este caso usaremos la base de datos de Chinook con este comando 
`USE Chinook` una vez lo hayamos escrito escribiremos `SHOW TABLES` con este comando podras ver todas las tablas de esa base de datos.

#### Cómo explorar los campos de una tabla y conocer los tipos de datos y las restricciones
Como antes usaremos la base de datos Chinook y escribiremos `DESCRIBE Album` y aparecerá el nombre de las tablas, el tipo, si es nulo, si es primary key, ...

#### Cómo gestionar las transacciones (COMMIT, ROLLBACK y AUTOCOMMIT)
`COMMIT` Sirve para confirmar los cambios realizados en una transacción.
`ROLLBACK` Sirve para revertir los cambios realizados en una transacción y volver al estado inicial.
`SET autocommit = 1` Está configurado para que cada declaración sea una transacción y para desactivarlo `SET autocommit = 0`



