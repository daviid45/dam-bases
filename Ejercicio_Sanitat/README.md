### UD_C1: DML - Consultas Reto 1: Consultas básicas

## Ejercicio 1

Hecho por: David Barberá Muñoz

Si quieres ver el repositorio, dale click aqui https://gitlab.com/daviid45/dam-bases/-/tree/main/Ejercicio_Sanitat

#### 1. Muestre los hospitales existentes (número, nombre y teléfono)
```
use sanitat;

select HOSPITAL_COD, NOM, TELEFON

from HOSPITAL;
```

#### 2. Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.
```
use sanitat;

select HOSPITAL_COD, NOM, TELEFON

from HOSPITAL

where NOM like '_a%';
```

#### 3. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.
```
use sanitat;

select HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM

from PLANTILLA;
```
#### 4. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.
```
use sanitat;

select HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM

from PLANTILLA

where TORN not like 'N';
```
#### 5. Muestre a los enfermos nacidos en 1960.
```
use sanitat;

select *

from MALALT

where year(DATA_NAIX) = 1960;
```
#### 6. Muestre a los enfermos nacidos a partir del año 1960.
```
use sanitat;

select *

from MALALT

where year(DATA_NAIX) >= 1960;
```


