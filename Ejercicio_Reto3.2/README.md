### Reto 3.2 DCL Control de acceso
#### Alumno: David Barberá Muñoz

Si quieres ver el repositorio, dale click aqui https://gitlab.com/daviid45/dam-bases/-/tree/main/Ejercicio_Reto3.2

(Como conectar una base de datos desde la terminal, ejemplo `mysql -u root -p -P 33006 -h 127.0.0.1`)
#### Cómo registrar nuevos usuarios, modificarlos y eliminarlos.
### Registrar nuevos usuarios
```sql
CREATE USER 'nombre_usuario'@'localhost' IDENTIFIED BY 'contraseña';
```
Con este comando puedes crear usuarios, en este caso se llama el usuario `nombre_usuario` y `localhost` se refiere al puerto donde se conecta, en este caso esta en localhost que es el puerto local.
Donde pone `contraseña` a la hora de crear al usuario deberemos de poner una contraseña en esa casilla.

### Modificar usuarios
```sql
ALTER USER 'nombre_usuario'@'localhost' IDENTIFIED BY 'nueva_contraseña';
```
Con este comando se puede modificar la `contraseña` de cualquier usuario.

### Eliminar usuarios
```sql
DROP USER 'nombre_usuario'@'localhost';
```
Con este comando podemos eliminar el usuario, hay que espacificar el `nombre de usuario` y el `puerto`.
Buscando información si se podia cambiar el nombre del usuario no se puede, pero puedes crear otro usuario con el nombre deseado.

#### Cómo se autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD.
Los usuarios se autentican mediante la contraseña creada anteriormente.
Uno de los métodos de autenticación por un metodo llamado `plugin`

#### Cómo mostrar los usuarios existentes y sus permisos.
### Mostrar usuarios existentes
```sql
SELECT user
FROM mysql.user
```
Con este comando podemos ver todos los usuarios existentes en nuestra base de datos.

### Ver los permisos de los usuarios
```sql
SHOW GRANTS FOR 'nombre_usuario'@'localhost';
```
Con este comando puedes ver los permisos que tiene `nombre_usuario`

Hay dos tipos de privilegios, dinámicos y estáticos.
Estáticos:

![alt text](image.png)

Dinámicos:

![alt text](image-1.png)

#### Qué permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD.

    SELECT: Ver datos.
    INSERT: Añadir nuevos datos.
    UPDATE: Modificar datos.
    DELETE: Borrar datos.
    CREATE: Crear tablas/bases.
    ALTER: Modificar estructuras.
    DROP: Borrar tablas/bases.
    GRANT: Dar permisos.

#### Qué permisos hacen falta para gestionar los usuarios y sus permisos.
Para gestionar los usuarios y sus permisos haría falta usar el `GRANT OPTION` y `CREATE USER`.
Estos privilegios te permitirán crear nuevos usuarios, asignar y revocar permisos, así como hacer cambios en los permisos de los usuarios existentes.

#### Posibilidad de agrupar los usuarios (grupos/roles/...).
```sql
CREATE ROLE grupo1;
```
Este comando crea un nuevo rol en MySQL.

```sql
GRANT grupo1 TO 'nombre_usuario'@'localhost';
```
Este comando asigna un rol a un usuario específico.

```sql
REVOKE grupo1 FROM 'nombre_usuario'@'localhost';
```
Este comando revoca un rol asignado previamente a un usuario.

#### Qué comandos usamos para gestionar los usuarios y sus permisos.
```sql
SELECT user, host FROM mysql.user;
```
Este comando muestra todos los `usuarios` existentes en MySQL.

```sql
SHOW GRANTS FOR 'nombre_usuario'@'localhost';
```
Este comando muestra los `privilegios` concedidos a un usuario específico.
