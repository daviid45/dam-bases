### UD_C1: DML - Consultas Reto 3: Consultas básicas con JOIN

## Ejercicio 3

Hecho por: David Barberá Muñoz 

Si quieres ver el repositorio, dale click aqui https://gitlab.com/daviid45/dam-bases/-/tree/main/Ejercicio_Reto3

### Videoclub

#### 1. Lista todas las películas del videoclub junto al nombre de su género.
```
use videoclub;
select p.Titol as titulo, g.Descripcio as genero
from PELICULA p inner join GENERE g where p.CodiGenere = g.CodiGenere;
```

#### 2. Lista todas las facturas de María.
```
use videoclub;
select f.*, dayname(f.Data)
from FACTURA f inner join CLIENT c on f.DNI = c.DNI 
where c.Nom like 'Maria %';
```

#### 3. Lista las películas junto a su actor principal.
```
use videoclub;
select a.*, p.Titol
from PELICULA p inner join ACTOR a on p.CodiActor = a.CodiActor 
```

#### 4. Lista películas junto a todos los actores que la interpretaron.
```
use videoclub;
select a.*, p.Titol, i.*
from PELICULA p inner join ACTOR a on p.CodiActor = a.CodiActor
inner join INTERPRETADA i on a.CodiActor = i.CodiActor
```

#### 5. Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.
```
use videoclub;
select p1.CodiPeli, p1.Titol, p2.SegonaPart, p2.Titol
from PELICULA p1
inner join PELICULA p2 on p1.CodiPeli = p2.CodiPeli
```
En el ejercicio 5, en la base de datos las segundas partes no pone el nombre.
